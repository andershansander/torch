package caves

import "github.com/go-gl/mathgl/mgl32"

type Cave struct {
	Dug []Area
}
type Area interface {
	FarLeft() mgl32.Vec3
	NearRight() mgl32.Vec3
	Center() mgl32.Vec3
}
