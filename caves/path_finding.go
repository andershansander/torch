package caves

import (
	"math"

	astar "github.com/beefsack/go-astar"
	"github.com/go-gl/mathgl/mgl32"
)

type AreaImpl struct {
	FarLeftLoc   mgl32.Vec3
	NearRightLoc mgl32.Vec3
	CenterLoc    mgl32.Vec3
	Neighbors    []Area
}

func (a *AreaImpl) FarLeft() mgl32.Vec3 {
	return a.FarLeftLoc
}
func (a *AreaImpl) NearRight() mgl32.Vec3 {
	return a.NearRightLoc
}
func (a *AreaImpl) Center() mgl32.Vec3 {
	return a.CenterLoc
}

func (t *AreaImpl) PathNeighbors() []astar.Pather {
	return []astar.Pather{}
}

func (t *AreaImpl) PathNeighborCost(to astar.Pather) float64 {
	return t.PathEstimatedCost(to)
}

func (t *AreaImpl) PathEstimatedCost(to astar.Pather) float64 {
	target := to.(*AreaImpl)
	return math.Sqrt(math.Pow(float64(t.Center().X()-target.Center().X()), 2) + math.Pow(float64(t.Center().Z()-target.Center().Z()), 2))
}
