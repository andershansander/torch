package caves

import "github.com/go-gl/mathgl/mgl32"

type builder interface {
	DigBetween(FarLeft, NearRight mgl32.Vec3) *caveBuilder
	FinishCave() Cave
}

type caveBuilder struct {
	areas []*AreaImpl
}

func (c *caveBuilder) DigBetween(FarLeft, NearRight mgl32.Vec3) *caveBuilder {
	center := mgl32.Vec3{(FarLeft.X() + NearRight.X()) / 2, (FarLeft.Y() + NearRight.Y()) / 2, (FarLeft.Z() + NearRight.Z()) / 2}
	neighbors := make([]Area, 0)

	newArea := &AreaImpl{FarLeft, NearRight, center, neighbors}
	for _, possibleNeighbor := range c.areas {
		if shareEdge(newArea, possibleNeighbor) {
			newArea.Neighbors = append(newArea.Neighbors, possibleNeighbor)
			possibleNeighbor.Neighbors = append(possibleNeighbor.Neighbors, newArea)
		}
	}
	c.areas = append(c.areas, newArea)
	return c
}

func (c *caveBuilder) FinishCave() Cave {
	cave := Cave{}
	dug := make([]Area, len(c.areas))
	for i, area := range c.areas {
		dug[i] = area
	}
	cave.Dug = dug
	return cave
}

func StartDigCave() builder {
	return &caveBuilder{areas: make([]*AreaImpl, 0)}
}

func shareEdge(a1, a2 Area) bool {
	shareXY := a1.FarLeft().Y() == a2.NearRight().Y() && a1.FarLeft().X() == a2.NearRight().X() || a1.NearRight().Y() == a2.FarLeft().Y() && a1.NearRight().X() == a2.FarLeft().X()
	shareZY := a1.FarLeft().Y() == a2.NearRight().Y() && a1.FarLeft().Z() == a2.NearRight().Z() || a1.NearRight().Y() == a2.FarLeft().Y() && a1.NearRight().Z() == a2.FarLeft().Z()

	return (shareXY && (between(a1.FarLeft().Z(), a2.FarLeft().Z(), a2.NearRight().Z()) || between(a1.NearRight().Z(), a2.FarLeft().Z(), a2.NearRight().Z()))) ||
		(shareZY && (between(a1.FarLeft().X(), a2.FarLeft().X(), a2.NearRight().X()) || between(a1.NearRight().X(), a2.FarLeft().X(), a2.NearRight().X())))
}

func between(value, min, max float32) bool {
	return value > min && value < max
}
