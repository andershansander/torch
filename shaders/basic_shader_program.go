package shaders

import (
	"fmt"
	"strings"

	"github.com/go-gl/gl/v3.3-core/gl"
	"github.com/go-gl/mathgl/mgl32"
)

var vertexShader = `
#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec2 aTexCoords;

out vec2 TexCoords;

out VS_OUT {
    vec3 FragPos;
    vec3 Normal;
    vec2 TexCoords;
} vs_out;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

uniform bool reverseNormals;

void main()
{
    vs_out.FragPos = vec3(model * vec4(aPos, 1.0));
    if(reverseNormals)
        vs_out.Normal = transpose(inverse(mat3(model))) * (-1.0 * aNormal);
    else
        vs_out.Normal = transpose(inverse(mat3(model))) * aNormal;
    vs_out.TexCoords = aTexCoords;
    gl_Position = projection * view * model * vec4(aPos, 1.0);
}
` + "\x00"

var fragmentShader = `
#version 330 core
out vec4 FragColor;

in VS_OUT {
    vec3 FragPos;
    vec3 Normal;
    vec2 TexCoords;
} fs_in;

uniform sampler2D diffuseTexture;
uniform samplerCube depthMap;

uniform vec3 lightPos;
uniform vec3 viewPos;

uniform float farPlane;
uniform bool shadows;


// array of offset direction for sampling
vec3 gridSamplingDisk[20] = vec3[]
(
   vec3(1, 1,  1), vec3( 1, -1,  1), vec3(-1, -1,  1), vec3(-1, 1,  1), 
   vec3(1, 1, -1), vec3( 1, -1, -1), vec3(-1, -1, -1), vec3(-1, 1, -1),
   vec3(1, 1,  0), vec3( 1, -1,  0), vec3(-1, -1,  0), vec3(-1, 1,  0),
   vec3(1, 0,  1), vec3(-1,  0,  1), vec3( 1,  0, -1), vec3(-1, 0, -1),
   vec3(0, 1,  1), vec3( 0, -1,  1), vec3( 0, -1, -1), vec3( 0, 1, -1)
);

float ShadowCalculation(vec3 fragPos)
{
    vec3 fragToLight = fragPos - lightPos;
    float currentDepth = length(fragToLight);
    float shadow = 0.0;
    float bias = 0.15;
    int samples = 20;
    float viewDistance = length(viewPos - fragPos);
    float diskRadius = (1.0 + (viewDistance / farPlane)) / 25.0;
    for(int i = 0; i < samples; ++i)
    {
        float closestDepth = texture(depthMap, fragToLight + gridSamplingDisk[i] * diskRadius).r;
        closestDepth *= farPlane;   // undo mapping [0;1]
        if(currentDepth - bias > closestDepth)
            shadow += 1.0;
    }
    shadow /= float(samples);
    return shadow;
}

void main()
{           
    vec3 color = texture(diffuseTexture, fs_in.TexCoords).rgb;
    vec3 normal = normalize(fs_in.Normal);
    vec3 lightColor = vec3(0.9, 0.6, 0.1);
    vec3 ambient = 0.01 * color;
    
    float distanceMultiplier = 1.0 / (length(fs_in.FragPos - lightPos) / 3);
    vec3 lightDir = normalize(lightPos - fs_in.FragPos);
    float diff = max(dot(lightDir, normal), 0.0);
    vec3 diffuse = diff * lightColor;
    diffuse = distanceMultiplier * diffuse * 0.8;
    
    vec3 viewDir = normalize(viewPos - fs_in.FragPos);
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = 0.0;
    vec3 halfwayDir = normalize(lightDir + viewDir);  
    spec = pow(max(dot(normal, halfwayDir), 0.0), 64.0);
    vec3 specular = 0.01 * spec * lightColor;    
    
    float shadow = shadows ? ShadowCalculation(fs_in.FragPos) : 0.0;                      
    vec3 lighting = (ambient + (1.0 - shadow) * (diffuse + specular)) * color;    
    
    FragColor = vec4(lighting, 1.0);
}
` + "\x00"

type basicShaderImpl struct {
	glId       uint32
	projection mgl32.Mat4
	view       mgl32.Mat4
	camera     mgl32.Vec3
}

func (s *basicShaderImpl) UniformPos(name string) int32 {
	return gl.GetUniformLocation(s.glId, gl.Str(name+"\x00"))
}
func (s *basicShaderImpl) GlId() uint32 {
	return s.glId
}
func (s *basicShaderImpl) SetMat(name string, data mgl32.Mat4) {
	gl.UniformMatrix4fv(s.UniformPos(name), 1, false, &data[0])
}
func (s *basicShaderImpl) SetVec(name string, data mgl32.Vec3) {
	gl.Uniform3fv(s.UniformPos(name), 1, &data[0])
}
func (s *basicShaderImpl) SetFloat(name string, data float32) {
	gl.Uniform1f(s.UniformPos(name), data)
}
func (s *basicShaderImpl) SetBool(name string, data bool) {
	var value = gl.FALSE
	if data {
		value = gl.TRUE
	}
	gl.Uniform1i(s.UniformPos(name), int32(value))
}
func (s *basicShaderImpl) SetInt(name string, data int32) {
	gl.Uniform1i(s.UniformPos(name), data)
}
func (s *basicShaderImpl) GetProjectionMatrix() mgl32.Mat4 {
	return s.projection
}
func (s *basicShaderImpl) GetViewMatrix() mgl32.Mat4 {
	return s.view
}
func (s *basicShaderImpl) GetCameraLocation() mgl32.Vec3 {
	return s.camera
}

func CreateBasicShader(windowWidth, windowHeight float32, cameraPos mgl32.Vec3) (BasicShader, error) {
	vertexShader, err := CompileShader(vertexShader, gl.VERTEX_SHADER)
	basicShader := &basicShaderImpl{}
	if err != nil {
		return basicShader, err
	}

	fragmentShader, err := CompileShader(fragmentShader, gl.FRAGMENT_SHADER)
	if err != nil {
		return basicShader, err
	}

	program := gl.CreateProgram()
	basicShader.glId = program
	gl.AttachShader(program, vertexShader)
	gl.AttachShader(program, fragmentShader)
	gl.LinkProgram(program)

	var status int32
	gl.GetProgramiv(program, gl.LINK_STATUS, &status)
	if status == gl.FALSE {
		var logLength int32
		gl.GetProgramiv(program, gl.INFO_LOG_LENGTH, &logLength)

		log := strings.Repeat("\x00", int(logLength+1))
		gl.GetProgramInfoLog(program, logLength, nil, gl.Str(log))

		return basicShader, fmt.Errorf("failed to link program: %v", log)
	}

	gl.DeleteShader(vertexShader)
	gl.DeleteShader(fragmentShader)

	gl.UseProgram(program)
	projection := mgl32.Perspective(mgl32.DegToRad(45.0), float32(windowWidth)/windowHeight, 0.1, 100.0)
	basicShader.SetMat("projection", projection)
	basicShader.projection = projection
	camera := mgl32.LookAtV(cameraPos, mgl32.Vec3{0, 0, 0}, mgl32.Vec3{0, 1, 0})
	basicShader.camera = cameraPos
	basicShader.view = camera
	basicShader.SetMat("view", camera)
	basicShader.SetVec("viewPos", basicShader.camera)
	basicShader.SetMat("model", mgl32.Ident4())
	basicShader.SetVec("lightPos", mgl32.Vec3{0, 0, 0})
	basicShader.SetBool("reverseNormals", false)
	basicShader.SetBool("shadows", true)
	basicShader.SetInt("diffuseTexture", 0)
	basicShader.SetInt("depthMap", 1)

	gl.BindFragDataLocation(program, 0, gl.Str("FragColor\x00"))
	return basicShader, nil
}
