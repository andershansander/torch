package main

import (
	"fmt"
	"log"
	"math"
	"runtime"

	"github.com/go-gl/mathgl/mgl32"

	"bitbucket.org/andershansander/torch/caves"
	"bitbucket.org/andershansander/torch/player"
	"bitbucket.org/andershansander/torch/shaders"
	"bitbucket.org/andershansander/torch/textures"
	"github.com/go-gl/gl/v3.3-core/gl"
	"github.com/go-gl/glfw/v3.2/glfw"
)

const (
	windowWidth  = 960
	windowHeight = 540
	shadowWidth  = 1024
	shadowHeight = 1024
)

func main() {
	runtime.LockOSThread()

	if err := glfw.Init(); err != nil {
		log.Fatalln("failed to initialize glfw:", err)
	}
	defer glfw.Terminate()

	glfw.WindowHint(glfw.Resizable, glfw.False)
	glfw.WindowHint(glfw.ContextVersionMajor, 3)
	glfw.WindowHint(glfw.ContextVersionMinor, 1)
	//glfw.WindowHint(glfw.OpenGLProfile, glfw.OpenGLCoreProfile)
	glfw.WindowHint(glfw.OpenGLForwardCompatible, glfw.True)
	window, err := glfw.CreateWindow(windowWidth, windowHeight, "Happy dala horse", nil, nil)
	nearPlane := float32(0.1)
	farPlane := float32(100.0)

	if err != nil {
		panic(err)
	}
	window.MakeContextCurrent()

	// Initialize Glow
	if err := gl.Init(); err != nil {
		panic(err)
	}

	version := gl.GoStr(gl.GetString(gl.VERSION))
	fmt.Println("OpenGL version", version)

	cameraPos := mgl32.Vec3{0, 8, 25}
	shader, err := shaders.CreateBasicShader(windowWidth, windowHeight, cameraPos)
	if err != nil {
		panic(err)
	}
	shadowShader, err := shaders.CreateShadowShader()
	if err != nil {
		panic(err)
	}

	gl.UseProgram(shadowShader.GlId())
	shadowShader.SetFloat("farPlane", farPlane)
	gl.UseProgram(shader.GlId())
	shader.SetFloat("farPlane", farPlane)

	gl.Enable(gl.DEPTH_TEST)
	//gl.Enable(gl.CULL_FACE)
	gl.ClearColor(0.0, 0.0, 0.0, 1.0)

	var depthMapFBO uint32
	gl.GenFramebuffers(1, &depthMapFBO)
	gl.BindFramebuffer(gl.FRAMEBUFFER, depthMapFBO)

	var depthCubemap uint32
	gl.GenTextures(1, &depthCubemap)
	gl.BindTexture(gl.TEXTURE_CUBE_MAP, depthCubemap)
	for i := uint32(0); i < 6; i++ {
		gl.TexImage2D(gl.TEXTURE_CUBE_MAP_POSITIVE_X+i, 0, gl.DEPTH_COMPONENT,
			shadowWidth, shadowHeight, 0, gl.DEPTH_COMPONENT, gl.FLOAT, nil)
	}
	gl.TexParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MAG_FILTER, gl.NEAREST)
	gl.TexParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MIN_FILTER, gl.NEAREST)
	gl.TexParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE)
	gl.TexParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE)
	gl.TexParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_R, gl.CLAMP_TO_EDGE)

	gl.FramebufferTexture(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, depthCubemap, 0)
	gl.DrawBuffer(gl.NONE)
	gl.ReadBuffer(gl.NONE)
	gl.BindFramebuffer(gl.FRAMEBUFFER, 0)

	// Load the texture
	texture, err := textures.NewTexture("textures/horse.png")
	if err != nil {
		log.Fatalln(err)
	}
	rockTexture, err := textures.NewTexture("textures/sten.jpg")
	if err != nil {
		log.Fatalln(err)
	}

	cave := caves.StartDigCave().
		DigBetween(mgl32.Vec3{-10, 0, -4}, mgl32.Vec3{10, 0, 4}).
		FinishCave()

	//cave rendering
	var verticeData = make([]float32, 0)
	for _, loc := range cave.Dug {
		vertexData := []float32{
			loc.FarLeft().X(), loc.FarLeft().Y(), loc.FarLeft().Z(), 0.0, 1.0, 0.0, 0.0, 0.0,
			loc.FarLeft().X(), loc.FarLeft().Y(), loc.NearRight().Z(), 0.0, 1.0, 0.0, 0.0, 1.0,
			loc.NearRight().X(), loc.FarLeft().Y(), loc.FarLeft().Z(), 0.0, 1.0, 0.0, 1.0, 0.0,

			loc.NearRight().X(), loc.FarLeft().Y(), loc.FarLeft().Z(), 0.0, 1.0, 0.0, 1.0, 0.0,
			loc.FarLeft().X(), loc.FarLeft().Y(), loc.NearRight().Z(), 0.0, 1.0, 0.0, 0.0, 1.0,
			loc.NearRight().X(), loc.FarLeft().Y(), loc.NearRight().Z(), 0.0, 1.0, 0.0, 1.0, 1.0,
		}
		verticeData = append(verticeData, vertexData...)
	}
	caveVao := createVertexArray(shader, verticeData)

	player := &player.Player{Angle: 3.1415 / 6, Velocity: 0.0, Position: mgl32.Vec3{1, 1, 1}, Target: mgl32.Vec3{1, 1, 1}}
	playerVao := createVertexArray(shader, cubeVertices)

	window.SetMouseButtonCallback(createMouseHandler(shader, cave, player))
	previousTime := glfw.GetTime()
	for !window.ShouldClose() {
		time := glfw.GetTime()
		elapsed := float32(time - previousTime)
		previousTime = time
		elapsed++
		if distance(player.Target.X(), player.Target.Z(), player.Position.X(), player.Position.Z()) > 0.1 {
			player.Angle = angleBetween(player.Target.X(), player.Target.Z(), player.Position.X(), player.Position.Z())
			xDiff, zDiff := offset(player.Angle, 0.005*elapsed)
			player.Position = mgl32.Vec3{player.Position.X() + xDiff, player.Position.Y(), player.Position.Z() + zDiff}
		}
		// 0. create depth cubemap transformation matrices
		// -----------------------------------------------
		shadowProj := mgl32.Perspective(mgl32.DegToRad(90.0), float32(shadowWidth)/float32(shadowHeight), nearPlane, farPlane)
		torchOffsetX, torchOffsetZ := offset(player.Angle, 1.1)

		lightPos := mgl32.Vec3{player.Position.X() + torchOffsetX, player.Position.Y() + 1.5, player.Position.Z() + torchOffsetZ}
		gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

		// 1. render scene to depth cubemap
		// --------------------------------
		gl.Viewport(0, 0, shadowWidth, shadowHeight)
		gl.BindFramebuffer(gl.FRAMEBUFFER, depthMapFBO)
		gl.Clear(gl.DEPTH_BUFFER_BIT)
		gl.UseProgram(shadowShader.GlId())
		shadowShader.SetMat("shadowMatrices[0]", shadowProj.Mul4(mgl32.LookAt(lightPos.X(), lightPos.Y(), lightPos.Z(), lightPos.X()+1, lightPos.Y(), lightPos.Z(), 0.0, -1.0, 0.0)))
		shadowShader.SetMat("shadowMatrices[1]", shadowProj.Mul4(mgl32.LookAt(lightPos.X(), lightPos.Y(), lightPos.Z(), lightPos.X()-1, lightPos.Y(), lightPos.Z(), 0.0, -1.0, 0.0)))
		shadowShader.SetMat("shadowMatrices[2]", shadowProj.Mul4(mgl32.LookAt(lightPos.X(), lightPos.Y(), lightPos.Z(), lightPos.X(), lightPos.Y()+1, lightPos.Z(), 0.0, 0.0, 1.0)))
		shadowShader.SetMat("shadowMatrices[3]", shadowProj.Mul4(mgl32.LookAt(lightPos.X(), lightPos.Y(), lightPos.Z(), lightPos.X(), lightPos.Y()-1, lightPos.Z(), 0.0, 0.0, -1.0)))
		shadowShader.SetMat("shadowMatrices[4]", shadowProj.Mul4(mgl32.LookAt(lightPos.X(), lightPos.Y(), lightPos.Z(), lightPos.X(), lightPos.Y(), lightPos.Z()+1, 0.0, -1.0, 0.0)))
		shadowShader.SetMat("shadowMatrices[5]", shadowProj.Mul4(mgl32.LookAt(lightPos.X(), lightPos.Y(), lightPos.Z(), lightPos.X(), lightPos.Y(), lightPos.Z()-1, 0.0, -1.0, 0.0)))
		shadowShader.SetVec("lightPos", lightPos)
		renderCave(player, cave, caveVao, shadowShader, lightPos)
		renderPlayer(player, playerVao, shadowShader, lightPos)
		gl.BindFramebuffer(gl.FRAMEBUFFER, 0)

		gl.UseProgram(shader.GlId())
		gl.Viewport(0, 0, windowWidth, windowHeight)
		gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
		gl.ActiveTexture(gl.TEXTURE0)
		gl.BindTexture(gl.TEXTURE_2D, rockTexture)
		gl.ActiveTexture(gl.TEXTURE1)
		gl.BindTexture(gl.TEXTURE_CUBE_MAP, depthCubemap)

		renderCave(player, cave, caveVao, shader, lightPos)
		gl.ActiveTexture(gl.TEXTURE0)
		gl.BindTexture(gl.TEXTURE_2D, texture)
		renderPlayer(player, playerVao, shader, lightPos)

		window.SwapBuffers()
		glfw.PollEvents()
	}
}

func distance(point1X, point1Y, point2X, point2Y float32) float32 {
	xDiff := point2X - point1X
	yDiff := point2Y - point1Y
	return float32(math.Sqrt(float64(xDiff*xDiff + yDiff*yDiff)))
}

func offset(angle, distance float32) (x, y float32) {
	x = float32(math.Cos(float64(angle))) * distance
	y = float32(math.Sin(float64(angle))) * distance
	return x, y
}

func angleBetween(point1X, point1Y, point2X, point2Y float32) float32 {
	return float32(math.Atan2(float64(point1Y-point2Y), float64(point1X-point2X)))
}

func createVertexArray(shader shaders.BasicShader, vertices []float32) uint32 {
	var vao uint32
	gl.GenVertexArrays(1, &vao)
	gl.BindVertexArray(vao)

	var vbo uint32
	gl.GenBuffers(1, &vbo)
	gl.BindBuffer(gl.ARRAY_BUFFER, vbo)
	gl.BufferData(gl.ARRAY_BUFFER, len(vertices)*4, gl.Ptr(vertices), gl.STATIC_DRAW)

	vertAttrib := uint32(gl.GetAttribLocation(shader.GlId(), gl.Str("aPos\x00")))
	gl.EnableVertexAttribArray(vertAttrib)
	gl.VertexAttribPointer(vertAttrib, 3, gl.FLOAT, false, 8*4, gl.PtrOffset(0))

	normalAttrib := uint32(gl.GetAttribLocation(shader.GlId(), gl.Str("aNormal\x00")))
	gl.EnableVertexAttribArray(normalAttrib)
	gl.VertexAttribPointer(normalAttrib, 3, gl.FLOAT, false, 8*4, gl.PtrOffset(3*4))

	texCoordAttrib := uint32(gl.GetAttribLocation(shader.GlId(), gl.Str("aTexCoords\x00")))
	gl.EnableVertexAttribArray(texCoordAttrib)
	gl.VertexAttribPointer(texCoordAttrib, 2, gl.FLOAT, false, 8*4, gl.PtrOffset(6*4))
	return vao
}

func renderPlayer(player *player.Player, playerVao uint32, shader shaders.BasicShader, lightPos mgl32.Vec3) {
	gl.UseProgram(shader.GlId())
	shader.SetVec("lightPos", lightPos)

	gl.BindVertexArray(playerVao)
	translate := mgl32.Translate3D(player.Position.X(), player.Position.Y()+0.5, player.Position.Z())
	translate = translate.Mul4(mgl32.HomogRotate3DY(player.Angle))
	shader.SetMat("model", translate.Mul4(mgl32.Scale3D(0.2, 0.2, 0.2)))

	gl.DrawArrays(gl.TRIANGLES, 0, 6*2*3)
}

func renderCave(player *player.Player, cave caves.Cave, caveVao uint32, shader shaders.BasicShader, lightPos mgl32.Vec3) {
	gl.UseProgram(shader.GlId())
	shader.SetMat("model", mgl32.Ident4())
	shader.SetVec("lightPos", lightPos)
	gl.BindVertexArray(caveVao)
	gl.DrawArrays(gl.TRIANGLES, 0, int32(len(cave.Dug)*2*3))
}

func createMouseHandler(shader shaders.BasicShader, cave caves.Cave, p *player.Player) func(window *glfw.Window, mb glfw.MouseButton, action glfw.Action, mkey glfw.ModifierKey) {
	return func(window *glfw.Window, mb glfw.MouseButton, action glfw.Action, mkey glfw.ModifierKey) {
		mX, mY := window.GetCursorPos()
		mouseX := float32(mX)
		mouseY := float32(mY)
		if mb == glfw.MouseButtonLeft && action == glfw.Press {
			width, height := window.GetSize()
			x := (2.0*mouseX)/float32(width) - 1.0
			y := 1.0 - 2.0*mouseY/float32(height)
			z := float32(-1)
			rayVec := mgl32.Vec4{x, y, z, float32(1)}
			rayVec = shader.GetProjectionMatrix().Inv().Mul4x1(rayVec)
			rayVec = mgl32.Vec4{rayVec.X(), rayVec.Y(), -1, 0}
			rayVec = shader.GetViewMatrix().Inv().Mul4x1(rayVec)
			rayVecWorld := mgl32.Vec3{rayVec.X(), rayVec.Y(), rayVec.Z()}.Normalize()

			for _, loc := range cave.Dug {
				intersect, target := intersectFloor(loc, shader.GetCameraLocation(), shader.GetCameraLocation().Add(rayVecWorld))
				if intersect {
					fmt.Println(target)
					p.Target = target
					break
				}
			}
		}
	}
}

//only works on floor tiles that are horizontal
func intersectFloor(area caves.Area, rayPt1, rayPt2 mgl32.Vec3) (bool, mgl32.Vec3) {
	squarePt1 := area.FarLeft()
	squarePt2 := mgl32.Vec3{area.NearRight().X(), area.FarLeft().Y(), area.FarLeft().Z()}
	squarePt3 := mgl32.Vec3{area.FarLeft().X(), area.FarLeft().Y(), area.NearRight().Z()}

	dS21 := squarePt2.Sub(squarePt1)
	dS31 := squarePt3.Sub(squarePt1)
	squareNormal := dS21.Cross(dS31)

	rayVec := rayPt1.Sub(rayPt2)
	ndotdR := squareNormal.Dot(rayVec)
	if math.Abs(float64(ndotdR)) < 1e-6 {
		return false, mgl32.Vec3{}
	}

	t := -squareNormal.Dot(rayPt1.Sub(squarePt1)) / ndotdR
	m := rayPt1.Add(rayVec.Mul(t))

	return m.X() > area.FarLeft().X() && m.X() < area.NearRight().X() && m.Z() > area.FarLeft().Z() && m.Z() < area.NearRight().Z(), m
	/*dMS1 := m.Sub(squarePt1)
	u := dMS1.Dot(dS21)
	v := dMS1.Dot(dS31)
	return u >= 0.0 && u <= dS21.Dot(dS21) && v >= 0.0 && v <= dS31.Dot(dS31)*/
}

var cubeVertices = []float32{
	//  X, Y, Z, normalX,Y,Z, U, V
	// Bottom
	-1.0, -1.0, -1.0, 0.0, -1.0, 0.0, 0.0, 0.0,
	1.0, -1.0, -1.0, 0.0, -1.0, 0.0, 1.0, 0.0,
	-1.0, -1.0, 1.0, 0.0, -1.0, 0.0, 0.0, 1.0,
	1.0, -1.0, -1.0, 0.0, -1.0, 0.0, 1.0, 0.0,
	1.0, -1.0, 1.0, 0.0, -1.0, 0.0, 1.0, 1.0,
	-1.0, -1.0, 1.0, 0.0, -1.0, 0.0, 0.0, 1.0,

	// Top
	-1.0, 1.0, -1.0, 0.0, 1.0, 0.0, 0.0, 0.0,
	-1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 0.0, 1.0,
	1.0, 1.0, -1.0, 0.0, 1.0, 0.0, 1.0, 0.0,
	1.0, 1.0, -1.0, 0.0, 1.0, 0.0, 1.0, 0.0,
	-1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 0.0, 1.0,
	1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 1.0, 1.0,

	// Front
	-1.0, -1.0, 1.0, 0.0, 0.0, 1.0, 1.0, 1.0,
	1.0, -1.0, 1.0, 0.0, 0.0, 1.0, 0.0, 1.0,
	-1.0, 1.0, 1.0, 0.0, 0.0, 1.0, 1.0, 0.0,
	1.0, -1.0, 1.0, 0.0, 0.0, 1.0, 0.0, 1.0,
	1.0, 1.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0,
	-1.0, 1.0, 1.0, 0.0, 0.0, 1.0, 1.0, 0.0,

	// Back
	-1.0, -1.0, -1.0, 0.0, 0.0, -1.0, 0.0, 0.0,
	-1.0, 1.0, -1.0, 0.0, 0.0, -1.0, 0.0, 1.0,
	1.0, -1.0, -1.0, 0.0, 0.0, -1.0, 1.0, 0.0,
	1.0, -1.0, -1.0, 0.0, 0.0, -1.0, 1.0, 0.0,
	-1.0, 1.0, -1.0, 0.0, 0.0, -1.0, 0.0, 1.0,
	1.0, 1.0, -1.0, 0.0, 0.0, -1.0, 1.0, 1.0,

	// Left
	-1.0, -1.0, 1.0, -1.0, 0.0, 0.0, 0.0, 1.0,
	-1.0, 1.0, -1.0, -1.0, 0.0, 0.0, 1.0, 0.0,
	-1.0, -1.0, -1.0, -1.0, 0.0, 0.0, 0.0, 0.0,
	-1.0, -1.0, 1.0, -1.0, 0.0, 0.0, 0.0, 1.0,
	-1.0, 1.0, 1.0, -1.0, 0.0, 0.0, 1.0, 1.0,
	-1.0, 1.0, -1.0, -1.0, 0.0, 0.0, 1.0, 0.0,

	// Right
	1.0, -1.0, 1.0, 1.0, 0.0, 0.0, 1.0, 1.0,
	1.0, -1.0, -1.0, 1.0, 0.0, 0.0, 1.0, 0.0,
	1.0, 1.0, -1.0, 1.0, 0.0, 0.0, 0.0, 0.0,
	1.0, -1.0, 1.0, 1.0, 0.0, 0.0, 1.0, 1.0,
	1.0, 1.0, -1.0, 1.0, 0.0, 0.0, 0.0, 0.0,
	1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 1.0,
}
