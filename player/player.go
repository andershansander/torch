package player

import "github.com/go-gl/mathgl/mgl32"

type Player struct {
	Position mgl32.Vec3
	Target   mgl32.Vec3
	Velocity float32
	Angle    float32
}
